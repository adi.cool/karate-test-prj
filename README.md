# Karate Test Automation Project(Java)

Java project with karate test automation implementation to execute test cases/features build in BDD methodology

```bash
mvn test
```

## Reports
- /target/karate-reports/src.test.java.features.Test.html
- /target/cucumber-html-reports/overview-features.html
- /target/karate-reports/karate-timeline.html

### Code references
https://github.com/karatelabs/karate