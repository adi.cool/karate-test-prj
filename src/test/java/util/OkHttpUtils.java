package util;

import okhttp3.OkHttpClient;

import javax.net.ssl.*;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;

public class OkHttpUtils {
    // https connection with proxy settings e.g. to get token
    public static OkHttpClient getUnsafeOkHttpClient(boolean followRedirect) {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }


                        @Override
                        public void checkServerTrusted(X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }


                        @Override public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };

            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
            okHttpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            okHttpClient.hostnameVerifier(new HostnameVerifier() {
                @Override public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            //okHttpClient.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("<url>",8080)));
            okHttpClient.followRedirects(followRedirect);
            okHttpClient.followSslRedirects(followRedirect);
            okHttpClient.callTimeout(Duration.ofSeconds(10));

            return okHttpClient.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
